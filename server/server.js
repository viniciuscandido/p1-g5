//Biblioteca TCP
net = require('net');
//Importa os outros módulos
Nick = require('./nick.js'); 
User = require('./user.js');

// Cria um vetor de clientes e duas listas dinâmicas. Uma nicks e users 
var clients = [];


// Cria uma lista dinâmica de usuários
var users = {};


var nicks = {}; 
var users = {};
// Matheus
// Cria um TCP server
net.createServer(function (socket) {
  // Coloca o nome do server como o endereço local + porta
  socket.name = socket.remoteAddress + ":" + socket.remotePort;
  // Coloca o cliente na lista de clientes (vetor)
  clients.push(socket);
  // Manda uma mensagem de boas vidas ao cliente que entrar
  socket.write("Seja bem vindo " + socket.name + "\n");
  // Manda a todos os clientes (broadcast) uma mensagem que o cliente entrou 
  broadcast(socket.name + " entrou no chat\n", socket);
  // Quem esta no servidor manda mensagem e todos recebem
  socket.on('data', function (data) {
	
   // broadcast(socket.name + "> " + data, socket);
	// verifica aqui o comando
	analisar(data);
  });
  // Remove o cliente quando deixa o chat
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
    broadcast(socket.name + " deixou o chat.\n");
  });
  
  // Definicao da funcao broadcast
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      // Nao mandar para o cliente que mandou a mensagem
      if (client === sender) return;
      client.write(message);
    });
    process.stdout.write(message)
  }
  // verificar a entrada de string para execucao dos comandos
  function analisar(data){
	var mensagem = String(data).trim();

	var args = mensagem.split(" ");
	if (args[0] == "NICK" ) nick(args);
	else if (args[0] == "USER") user(args);
	else if (args[0] == "JOIN") join(args);
	else socket.write("Comando inexistente. Sintaxe: NICK, USER ou JOIN");
	}

  function nick(args){
	socket.write("OK, executado!\n");
	}
  function user(args){
	// validação inicial dos parâmetros
	if ((typeof args[1] == "undefined") ||
	    (typeof args[2] == "undefined") ||
	    (typeof args[3] == "undefined") ||
	    (typeof args[4] == "undefined")) {
		socket.write("Parâmetros inválidos!\n");
		return;
	}
	
	if(users[ args[1]]) {
		socket.write("Username já utilizado!\n");
		return;
	}
	
	if (socket.username ) delete users[socket.username];
	socket.username = args[1];
	
	// valida se o parâmetro do modo é um número
	if(isNaN(args[2])){
		socket.write("Modo informado é inválido!\n");
		return;
	}	
	socket.mode = args[2];

	// validação e atribuição do nome completo.
	var nomeCompleto = args.join(" ");
	nomeCompleto = nomeCompleto.split(":")[1];
	if (typeof nomeCompleto == "undefined"){
		socket.write("Nome completo inválido!\n");
		return;
	}	
	socket.realname = nomeCompleto.trim();
	
	users[args[1]] = socket.name;
	socket.write("O usuário " + socket.name + " agora é " + socket.username + "\n");
	socket.write("Modo: " + socket.mode + " Nome completo: " + socket.realname + "\n");
  }

	var mensagem = mensagem.toUpperCase()
	var args = mensagem.split(" "); 
	if (args[0] == "NICK" ) Nick(args, socket, nicks); //Chama a função que está no módulo nick.js
	else if (args[0] == "USER") User(args, socket, users); //Chama a função que está no módulo user.js
	else if (args[0]  == "JOIN") join(args);
	else socket.write("Comando inexistente. Sintaxe: NICK, USER ou JOIN\n");
	}
  	
// Matheus
  function join(args){
	socket.write("Ok, executado!\n");
	}
}).listen(6000);
// Mensagem inicial do chat 
console.log("Chat na porta 6000\n");
