## USER
### 	USER <*user*> <*mode*> <*unused*> <*realname*> 
### 	Este comando é utilizado no início da conexão (logo após executar o telnet) para definir o nome de usuário que está se conectando, o modo de conexão e o nome real do usuário.
### 	USER ygorats * * Ygor Alberto Terra dos Santos
### Código
```
socket.on('data', function (data) {
	var mensagem = String(data).trim();
        var args = String(mensagem).split(" ");
	if(args[0] == "USER") user(args);
	else broadcast(socket.username + "> " + data, socket);
  });

  function user(args) {
	
      if (typeof args[1] != "undefined") {
	if(users[ args[1]])
		socket.write("Username já utilizado!\n")
	else {
		if (socket.username ) delete users[socket.username];
		socket.username = args[1];
		socket.mode = args[2];

		var i = 4;
		socket.realname = "";
		while(typeof args[i] != "undefined"){
			socket.realname = socket.realname + args[i] + " ";
			i++;
		}
		users[args[1]] = socket.name;
		socket.write("O usuário " + socket.name + " agora é " + socket.username + "\n");
		socket.write("Modo: " + socket.mode + " Nome completo: " + socket.realname + "\n");
	}
      }

      else socket.write("Parâmetros inválidos!\n");
  }
```

