## Join
	### /join ( <channel> *( "," <channel> ) [ <key> *( "," <key> ) ] ) / "0"
	### O comando join é usado pelo usuário para requisitar a começar ouvir um canal especifico. Os servidores devem ser capazes de analizar os argumentos na de forma uma lista, mas não deve usar listas para enviar mensagens do join para os clientes.
	### Uma vez que o cliente entra num canal ele passa a receber informações dos comandos que afetam o mesmo, como novos JOIN, MODE, KICK, PART, QUIT e PRIVMSG / NOTICE. Permitindo que os membros do canal acompanhe tudo que se passa no canal.
	### Se o JOIN for um sucesso, o usuário recebe uma mensagem de confirmação.
	### código: 'join':
		function (client, channel) {
			var ch = server.channels.list[channel],
				scand;
			if (!ch) {
				ch = server.channels.negotiate('create', channel, client);
			}
			scand = client.name.toScandinavianLowerCase();
			if (ch.users[scand]) {
				return;
			}
			ch.users[scand] = client;
			client.channels[channel] = ch;
			server.channels.negotiate('deliver', channel, client, 'JOIN',
					[channel]);
		}
